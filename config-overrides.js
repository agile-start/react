const path = require('path');

module.exports = function override(config, env){
  config.resolve.alias = {
    components : path.resolve(__dirname, 'src/components'),
    config : path.resolve(__dirname, 'src/config'),
    images : path.resolve(__dirname, 'src/images'),
    library : path.resolve(__dirname, 'src/library'),
    pages : path.resolve(__dirname, 'src/pages'),
    styles : path.resolve(__dirname, 'src/styles')
  };

  return config;
}
