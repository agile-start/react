import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Apod = ({ data, onLoadImage, className }) => {
  const {
    copyright, explanation, title, url,
  } = data;

  return (
    <div className={`component-apod ${className}`}>
      <h2>{title}</h2>

      <figure>
        <img src={url} alt="Of the day" onLoad={onLoadImage} />
        <figcaption>
          Copyright: {copyright}
        </figcaption>
      </figure>

      <p>{explanation}</p>
    </div>
  );
};

Apod.propTypes = {
  data: PropTypes.shape({
    copyright: PropTypes.string,
    explanation: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
  onLoadImage: PropTypes.func,
};

Apod.propValues = {
  className: '',
  onLoadImage: () => {},
};

export default Apod;
