import React from 'react';

import imgApod from 'images/nasa-apod.jpg';

import './style.scss';

const Footer = () => (
  <footer className="component-footer">
    <img src={ imgApod } alt="Nasa apod logo" />
  </footer>
);

export default Footer;
