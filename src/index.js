import React from 'react';
import { render } from 'react-dom';

import Page from 'pages/main';

import registerServiceWorker from './registerServiceWorker';

render(
  <Page />,
  document.getElementById('root'),
);

registerServiceWorker();
