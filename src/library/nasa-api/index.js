import axios from 'axios';

import env from 'config/env';

export function apod() {
  const apiUrl = `${env.REACT_APP_NASA_API_HOST}planetary/apod?api_key=${env.REACT_APP_NASA_API_KEY}`;

  return axios.get(apiUrl)
    .then(dataRequest => dataRequest.data);
}

export default {
  apod,
};
