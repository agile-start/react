import React from 'react';
import classNames from 'classnames';

import nasaApi from 'library/nasa-api';

import Header from 'components/header';
import Footer from 'components/footer';
import Apod from 'components/apod';
import Loading from 'components/loading';

import './style.scss';

class PageMain extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      apod: {},
      loading: true,
    };
  }

  updateDone() {
    this.setState({ loading: false });
  }

  componentWillMount() {
    nasaApi.apod().then((data) => {
      this.setState({ apod: data });
    });
  }

  render() {
    const { apod } = this.state;

    const pageClassNames = {
      'page-main': true,
      loading: this.state.loading,
    };

    return (
      <div className={classNames(pageClassNames)}>
        <Header />
        <Apod data={apod} className="page-main__apod" onLoadImage={this.updateDone.bind(this)} />
        <Loading className="page-main__loading page-main__loading" />
        <Footer />
      </div>
    );
  }
}

export default PageMain;
